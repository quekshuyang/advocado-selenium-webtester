package main

import (
    "fmt"
    "net/http"
    "bufio"
    "os"
    "time"
    "bitbucket.org/tebeka/selenium"
)

func main() { 
    http.HandleFunc("/", hello)
    http.HandleFunc("/screenshot", captureAdvocado)
    fmt.Println("listening...")
    err := http.ListenAndServe(":"+os.Getenv("PORT"), nil)
    if err != nil {
        panic(err)
    }
}

func hello(res http.ResponseWriter, req *http.Request) { 
    fmt.Fprintln(res, "Hello, World")
}


/**
 Goes to a site and activates Advocado and takes screenshots
 TODO: can figure out how to use goroutines to make the screenshot
 process faster.
*/
func captureAdvocado(res http.ResponseWriter, req *http.Request) { 

    url := req.FormValue("page_url")
    fmt.Println("page_url = " + url)

    if url != "" {
        // FireFox driver without specific version
        caps := selenium.Capabilities{"browserName": "firefox"}
        wd, err := selenium.NewRemote(caps, "http://syquek:f957a1be-2346-429d-aa9e-09f6dc0bb880@ondemand.saucelabs.com:80/wd/hub")
        if err != nil { panic(err) }
        defer wd.Quit()

        // Set a generous wait time (since we're calling via ajax anyway)
        wd.SetImplicitWaitTimeout(5000)

        wd.Get(url)

        // Find our button
        btn, _ := wd.FindElement(selenium.ByCSSSelector, "#advoc-discount-btn")

        // Take a screenshot of how the page looks
        s1, err := wd.Screenshot()

        btn.Click()

        // wait a bit for the reaction
        time.Sleep(1000 * time.Millisecond)

        // take another screenshot
        s2, err := wd.Screenshot()

        // write the bytes to a file
        fo1, err := os.Create("screenshot1.png")
        if err != nil { panic(err) }
        defer func() {
            if err := fo1.Close(); err != nil {
                panic(err)
            }
        }()

        fo2, err := os.Create("screenshot2.png")
        if err != nil { panic(err) }
        defer func() {
            if err := fo2.Close(); err != nil {
                panic(err)
            }
        }()

        w1 := bufio.NewWriter(fo1)
        w2 := bufio.NewWriter(fo2)

        if _, err := w1.Write(s1); err != nil {
            panic(err)
        }

        if _, err := w2.Write(s2); err != nil {
            panic(err)
        }

        // send it over boys
    }
}
