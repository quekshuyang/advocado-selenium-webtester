
# Advocado Selenium Checker

Wrote a tool in Go that uses the go version of the Selenium webdriver to capture screenshots of Advocado product pages.

##Dependencies

Needs the go selenium webdriver. Run:

```
go get bitbucket.org/tebeka/selenium
```
